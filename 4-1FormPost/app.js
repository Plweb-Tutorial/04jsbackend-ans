const PORT = process.env.PORT || 3000;

var express = require("express");
var path = require("path");
var app = express();
//parser body json values when form post to server
var bodyParser = require("body-parser");

var indexRoute = require("./routes/index");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, 'views'));
app.use(express.static(__dirname + "/public"));

app.use("/", indexRoute);

app.listen(PORT, function(){
  console.log("Starting the server port " + PORT);
})