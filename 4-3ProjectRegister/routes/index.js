var express = require("express");
var router = express.Router();

router.get("/", function(req, res){
    res.render("landing");
});

router.get("/aboutus", function(req, res){
    res.render("aboutus");
});

router.get("/ingredient", function(req, res){
    res.render("ingredient");
});

router.get("/register", function(req, res){
   res.render("register"); 
});

router.post("/register", function(req, res){
    var username = req.body.username;
    var email = req.body.email;
    var password = req.body.password;

    console.log(req.body);
    console.log(username, email, password);
    res.render("register"); 
});

router.get("/login", function(req, res){
   res.render("login"); 
});

module.exports = router;